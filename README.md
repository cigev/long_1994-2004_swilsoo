# SWILSOO

> État: commit initial, données à nettoyer.

Swilsoo – acronyme de Swiss Interdisciplinary Longitudinal Study on the Oldest Old – est une recherche longitudinale dont les objectifs principaux sont d'explorer les trajectoires de vie et de santé au cours de la grande vieillesse, les formes d'organisation de la vie au grand âge, les événements, les perturbations ainsi que les régulations mises en place par les vieillards et par leur entourage.

Ce projet a été financé par le Fonds national suisse de la recherche scientifique ([FNS](http://www.snf.ch/)).

## Pour commencer

Nous proposons ici un jeu de données mis à jour et corrigé en 2021, ainsi qu'une documentation complète. La liste des fichiers disponibles est [détaillée ci-dessous](#contenu), et la documentation se trouve dans le fichier [long_1994-2004_doc.md](long_1994-2004_doc.md).

En clonant ce dépôt, vous vous engagez à respecter la [licence d'utilisation](LICENSE) et à citer [les auteur·e·s](#auteures) et le titre de l'enquête ([voir la citation proposée](#citation)).

### Citation

(FR)
> Lalive d'Épinay, C. (dir.). (2004). *SWILSOO* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Lalive d'Épinay, C. (Dir.). (2017). *SWILSOO* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

### Prérequis

Vous pouvez télécharger les données fournies par le CIGEV et les utiliser librement, selon la [licence d'utilisation](LICENSE). Afin de faire profiter les autres utilisateurs/trices de vos corrections éventuelles, utilisez Git: voir le livre [Pro Git](https://git-scm.com/book/fr/v2) pour bien commencer.

Le [jeu de données](long_1994-2004_data.csv) est fourni au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values), mais peut être importé dans [SPSS](https://fr.wikipedia.org/wiki/SPSS) grâce à la [syntaxe d'importation](long_1994-2004_data.sps) qui l'accompagne. La [documentation](long_1994-2004_doc.md) est fournie sous la forme d'un fichier [MD](https://fr.wikipedia.org/wiki/Markdown). Le [codebook](long_1994-2004_codebook.ods) a été exporté depuis SPSS et est fourni au format [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument). Les questionnaires sont au format [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format).

### Installation

Vous pouvez copier le jeu de données et la documentation en le téléchargeant depuis ce site (voir lien «Download» en haut à droite de la liste des fichiers) ou en clonant le dépôt:

```
git clone git@gitlab.unige.ch:cigev/long_1994-2004_swilsoo.git
```

Pour importer les données dans SPSS, utilisez la syntaxe fournie (long_1994-2004_data.sps) et modifiez la ligne appelant le fichier CSV en indiquant le chemin complet du fichier.

## Contenu

* [LICENSE](LICENSE) – Licence d'utilisation des données
* [README.md](README.md) – Indications de base (ce fichier)
* xxx

## Contribuer

Si vous désirez contribuer à l'amélioration de ce jeu de données, de sa documentation, ou laisser des commentaires, vous pouvez contacter le CIGEV à l'adresse: [cigev-data@unige.ch](mailto:cigev-data@unige.ch). Le système Git vous permettra de proposer des modifications que nous pourrons valider, documenter et partager.

Pour toute utilisation des données (analyse, comparaison, enseignement, publication...), nous vous serions reconnaissants de bien vouloir nous informer: [cigev-data@unige.ch](mailto:cigev-data@unige.ch).

## Auteur·e·s

* **Christian Lalive d'Épinay** – *Requérant principal*
* **Jean-Pierre Michel**
* **Charles-Henri Rapin**
* **Alain Clémence**
* **Anik de Ribaupierre**
* **Gabrielle Antille Gaillard**

### Collaboratrices et collaborateurs

* **Franca Armi**
* **Jean-François Bickel**
* **Stefano Cavalli**
* **Christian Cordonier**
* **Nathalie Ducommun**
* **Paolo Ghisletta**
* **Myriam Girardin**
* **Luc Guillet**
* **Edith Guilley**
* **Karine Henchoz**
* **Cornelia Hummel**
* **Carole Maystre**
* **Stéphanie Pin**
* **Jean-François Riand**
* **Dario Spini**
* **Barbara Vascotto**
* **Nathalie Vollenwyder**

### Administration et technique

* **Nathalie Blanc**
* **Claire Grela**
* **Grégoire Métral**
* **Marie Périat**

## Licence

Ce projet est distribué selon les termes de la licence de CreativeCommons CC-BY-SA. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

## Remerciements

Merci à toutes les personnes (notamment les enquêtrices et enquêteurs) qui ont également collaboré à cette recherche.
